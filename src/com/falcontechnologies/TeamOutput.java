package com.falcontechnologies;

import java.util.Map;
import javax.swing.*;

/**
 *
 * @author Joon Bang
 */
public class TeamOutput extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	final static int WIDTH = 400;
    final static int HEIGHT = 600;

    /**
     * Initializes the fields. Creates a <code>JScrollPane</code> containing a <code>TeamPanel</code> object.
     *
     * @param projectTeams      2D <code>String</code> array of student IDs, arranged into project teams.
     * @param name              name of the project.
     * @param idsToName         <code>Map</code> of student IDs to their name; Last, First.
     */
    public TeamOutput(String[][] projectTeams, String name, 
            Map<String, String> idsToName) {
        TeamPanel panel = new TeamPanel(projectTeams, name, idsToName);
        
        JScrollPane pane = new JScrollPane(panel);
        pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        this.add(pane);
        
        initialize();
    }

    // sets properties of the JFrame
    private void initialize() {
        this.setSize(WIDTH, HEIGHT);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
}