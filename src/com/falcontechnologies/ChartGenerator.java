package com.falcontechnologies;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Joon Bang
 */
public class ChartGenerator extends GroupGenerator {

    // fields
    private final String fileName;

    private final int x;
    private final int y;

    private String[][] seatingChart;

    /**
     * Initializes <code>ChartGenerator</code> object; assigns values to all fields.
     *
     * @param fileName name of file where information about previous charts is stored.
     * @param ids      list of student IDs.
     * @param x        number of tables.
     * @param y        number of students per table.
     * @see com.falcontechnologies.GroupGenerator
     */
    public ChartGenerator(String fileName, String[] ids, int x, int y) {
        super(ids);
        this.fileName = fileName;
        this.seatingChart = new String[x][y];
        this.x = x;
        this.y = y;
    }

    /**
     * Generates an alphabetical seating chart.
     *
     * @return the generated seating chart.
     * @see    com.falcontechnologies.ChartOutput
     */
    public String[][] generateAlphabeticalChart() {
        // marks certain seats as empty; attempts to evenly distribute between different files
        for (int j = y - 1, c = 0; j >= 0; j--) {
            for (int i = x - 1; i >= 0 && c < 30 - ids.size(); i--) {
                seatingChart[i][j] = "Empty";
                c++;
            }
        }

        List<String> copy = new ArrayList<>(ids);

        // fills in other seats
        for (int i = 0; i < 30; i++) {
            if (!"Empty".equals(seatingChart[i / 5][i % 5]))
                seatingChart[i / 5][i % 5] = copy.remove(0);
        }

        return seatingChart.clone();
    }

    /**
     * Generates a random seating chart, with no redundancy; students sit next to students
     * they have not sat with previously.
     *
     * @return the generated seating chart.
     * @see    com.falcontechnologies.ChartOutput
     */
    public String[][] generateRandomChart() {
        String[][] newChart = seatingChart.clone();
        Map<String, List<String>> whiteList = generateMap(fileName);

        List<String> remaining = new ArrayList<>(ids);

        for (int i = 0; i < x; i++) {
            List<String> possibilities = new ArrayList<>(remaining);

            int j = 0, attempts = 0;
            while (j < y && !newChart[i][j].equals("Empty")) {
                if (attempts > 10000000 || possibilities.isEmpty())
                    return generateRandomChart();

                String next = getRandom(possibilities);

                if (j == 0 || remaining.contains(next)) {
                    newChart[i][j] = remaining.remove(remaining.indexOf(next));
                    possibilities = intersection(possibilities, whiteList.get(newChart[i][j]));
                    attempts = 0;
                } else {
                    attempts += 1;
                    continue;
                }

                j++;
            }
        }

        updateCSV(fileName, ids, newChart);

        seatingChart = newChart;
        return seatingChart.clone();
    }

}