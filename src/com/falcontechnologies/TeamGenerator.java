package com.falcontechnologies;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Joon Bang
 */
public class TeamGenerator extends GroupGenerator {

    // fields
    private final String fileName;
    
    private final int x;
    private final int y;

    private String[][] projectTeams;
    private final List<String>[] groups;

    /**
     * Initializes <code>TeamGenerator</code> object; assigns values to all fields.
     *
     * @param fileName name of file where information about previous teams is stored.
     * @param ids      list of student IDs.
     * @param x        number of tables.
     * @param y        number of students per table.
     * @see com.falcontechnologies.GroupGenerator
     */
    @SuppressWarnings("unchecked")
    public TeamGenerator(String fileName, String[] ids, int x, int y) {
        super(ids);
        
        this.fileName = fileName;
        this.projectTeams = new String[x][y];
        this.groups = new ArrayList[3];
        this.x = x;
        this.y = y;

        initialize();
    }

    private void initialize() {
        int c = (x * y - ids.size()) % y;
        for (int i = this.x - 1; c > 0; i--) {
            projectTeams[i][y - 1] = "Empty";
            c--;
        }
        
        ArrayList<String> copy = new ArrayList<>(this.ids);
        for (int i = 0; i < x * y; i++) {
            if (!"Empty".equals(projectTeams[i / y][i % y]))
                projectTeams[i / y][i % y] = copy.remove(0);
        }
    }

    /**
     * Generates a set of random project teams with no redundancy; students work with students
     * they have not worked with previously.
     *
     * @return the generated project teams.
     * @see    com.falcontechnologies.TeamOutput
     */
    public String[][] generateRandomTeams() {
        String[][] newTeams = projectTeams.clone();
        Map<String, List<String>> whiteList = generateMap(fileName);

        List<String> remaining = new ArrayList<>(ids);

        for (int i = 0; i < x; i++) {
            List<String> possibilities = new ArrayList<>(remaining);

            int j = 0, attempts = 0;
            while (j < y && !newTeams[i][j].equals("Empty")) {
                if (attempts > 10000000 || possibilities.isEmpty())
                    return generateRandomTeams();

                String student = getRandom(possibilities);

                if (j == 0 || remaining.contains(student)) {
                    newTeams[i][j] = remaining.remove(remaining.indexOf(student));
                    possibilities = intersection(possibilities, whiteList.get(newTeams[i][j]));
                    attempts = 0;
                } else {
                    attempts += 1;
                    continue;
                }

                j++;
            }
        }

        updateCSV(fileName, ids, newTeams);

        projectTeams = newTeams;
        return projectTeams.clone();
    }

    /**
     * Generates a set of project teams randomly with no redundancy, as per <code>generateRandomTeams()</code>;
     * in addition, teams are generated to be balanced such that each member is from a particular skill group.
     *
     * @return the generated teams.
     * @see    com.falcontechnologies.TeamOutput
     * @see    #assignStudentGroups(List[])
     */
    public String[][] generateTeamsBySkill() {
        String[][] newTeams = projectTeams.clone();
        Map<String, List<String>> whiteList = generateMap(fileName);

        List<String> remaining = new ArrayList<>(ids);
        List<String>[] studentGroups = groups.clone();

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                List<String> shared = new ArrayList<>(studentGroups[j]);

                for (int k = 0; k < j; k++)
                    if (!newTeams[i][k].equals("Empty"))
                        shared = intersection(shared, whiteList.get(newTeams[i][k]));

                if (shared.isEmpty())
                    return generateTeamsBySkill();

                newTeams[i][j] = studentGroups[j].remove(studentGroups[j].indexOf(getRandom(shared)));

                if (!newTeams[i][j].equals("Empty"))
                    remaining.remove(remaining.indexOf(newTeams[i][j]));
            }
        }

        updateCSV(fileName, ids, newTeams);

        projectTeams = newTeams;
        return projectTeams.clone();
    }

    /**
     * Assign students to skill groups, as per <code>subjects</code>, and attempt to balance out the
     * skill groups in terms of numbers. If the size of the skill groups are not equal despite attempts to balance,
     * the groups will be padded with "Empty".
     *
     * @param subjects      the groups of students by subject they are skilled at.
     * @see #generateTeamsBySkill()
     */
    public void assignStudentGroups(List<String>[] subjects) {
        for (int i = 0; i < 3; i++)
            groups[i] = new ArrayList<>(subjects[i]);

        // even out the people in each group
        int average = ids.size() / 3;

        List<String> floaters = new ArrayList<>();
        for (List<String> group : groups)
            while (group.size() > average)
                floaters.add(group.remove(group.size() - 1));

        for (List<String> group : groups)
            while (group.size() < average)
                group.add(floaters.remove(0));

        int c = 0;
        while (!floaters.isEmpty()) {
            groups[c % 3].add(floaters.remove(0));
            c++;
        }
        
        // even out with "Empty"
        int max = Math.max(Math.max(groups[0].size(), groups[1].size()), 
                groups[2].size());

        for (List<String> array : groups)
            for (int i = array.size(); i < max; i++)
                array.add("Empty");
    }

}