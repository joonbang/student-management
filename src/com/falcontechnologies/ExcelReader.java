package com.falcontechnologies;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 *
 * @author Joon Bang
 */
public class ExcelReader {

    // fields
    private final String inputFile;

    /**
     * Initializes the <code>inputFile</code> field.
     * @param inputFile     Excel file location.
     */
    public ExcelReader(String inputFile) {
        this.inputFile = inputFile;
    }

    /**
     * Converts data from specific sheet of an Excel spreadsheet (<code>.xls</code> file) to a  <code>.csv</code> file.
     * Note that no <code>.csv</code> file will be created.
     *
     * @param sheetName         name of Excel spreadsheet.
     * @return                  spreadsheet encoded as CSV.
     * @throws IOException      if the spreadsheet does not exist.
     */
    public String read(String sheetName) throws IOException {
        String text = "";
        POIFSFileSystem fs = new POIFSFileSystem(new BufferedInputStream(
                new FileInputStream(inputFile)));
        HSSFWorkbook wb = new HSSFWorkbook(fs);
        
        HSSFSheet sheet = null;
        for (int i = 0; i < wb.getNumberOfSheets(); i++) {
            if (sheetName.equals(wb.getSheetAt(i).getSheetName())) {
                sheet = wb.getSheetAt(i);
                break;
            }
        }
        
        if (sheet == null) {
            throw new IOException("Sheet DNE");
        }
        
        Iterator<?> rows = sheet.rowIterator();
        rows.next(); // remove the first row
        while (rows.hasNext()) {
            HSSFRow row = (HSSFRow) rows.next();
            Iterator<?> cells = row.cellIterator();
            while (cells.hasNext()) {
                HSSFCell cell = (HSSFCell) cells.next();
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    text += cell.getStringCellValue().trim() + ";";
                } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                    text += (int) cell.getNumericCellValue() + "\n";
                }
            }
        }

        return text;
    }
}
