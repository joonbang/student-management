package com.falcontechnologies;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class TextFile {

    private String fileName;

    public TextFile(String fileName) {
        this.fileName = fileName;
    }

    public String read() {
        String line;
        String result = "";

        try {
            BufferedReader reader = new BufferedReader(
                    new FileReader(fileName));
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }
            reader.close();

        } catch (FileNotFoundException e) {
            System.out.println("Could not find file '"
                    + fileName + "'.");
        } catch (IOException e) {
            System.out.println("Error reading file '"
                    + fileName + "'.");
        }

        return result;
    }

    public void write(String text) {
        List<String> lines = Arrays.asList(text.split("\n")); //copy pasted code
        Path file = Paths.get(fileName);
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println("Error writing to file '" + fileName + "'.");
        }
    }
}
