package com.falcontechnologies;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 *
 * @author Joon Bang
 */
class TeamPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	private final int WIDTH = TeamOutput.WIDTH;
    private final int HEIGHT = TeamOutput.HEIGHT;

    private final int SPACING = 10;

    private String[][] projectTeams;
    private String projectName;

    private FontMetrics fm;

    private int preferredHeight;

    /**
     * Initializes the fields.
     *
     * @param projectTeams      2D <code>String</code> array of Student IDs.
     * @param projectName       name of the project.
     * @param idsToName         <code>Map</code> of student IDs to their name; Last, First.
     * @see                     com.falcontechnologies.TeamOutput
     */
    public TeamPanel(String[][] projectTeams, String projectName,
                     Map<String, String> idsToName) {
        this.projectTeams = projectTeams.clone();
        this.projectName = projectName;

        for (String[] projectTeam : projectTeams) {
            for (int j = 0; j < projectTeam.length; j++) {
                projectTeam[j] = idsToName.get(projectTeam[j]);
            }
        }

        initialize();
    }

    // sets properties of the JPanel
    private void initialize() {
        this.setSize(WIDTH, HEIGHT);
    }

    /**
     * Gives the preferred size of the JPanel.
     *
     * @return the dimensions of the window.
     */
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(WIDTH, preferredHeight);
    }

    /**
     * Displays the project teams on the window using <code>paint</code>.
     *
     * @param g     Graphics of window.
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setFont(new Font("Tahoma 11 Plain", Font.BOLD, 22));
        fm = getFontMetrics(g.getFont());
        String[] teamNames = new String[projectTeams.length];

        if (projectName.equals("Chem R&E")) {
            teamNames = new TextFile("files/project_names.txt").read().split("\n");
        } else {
            for (int i = 0; i < teamNames.length; i++) {
                teamNames[i] = "Team #"+(i+1);
            }
        }

        g.drawString(projectName + " Teams", SPACING, fm.getHeight());

        int y = fm.getHeight() + SPACING;

        g.setFont(new Font("Tahoma 11 Plain", 0, 18));
        fm = getFontMetrics(g.getFont());

        y += fm.getHeight();

        for (int i = 0; i < projectTeams.length; i++) {
            int x = 2 * SPACING;
            g.drawString(teamNames[i], x, y);

            x += SPACING;
            for (String name : projectTeams[i]) {
                y += fm.getHeight();
                if (name.equals("")) {
                    y -= fm.getHeight();
                    break;
                }

                g.drawString(name, x, y);
            }

            y += 3 * SPACING;
        }

        this.preferredHeight = y;
    }
}