package com.falcontechnologies;

import java.util.*;

public class GroupGenerator {

    // constants
    private static final Random R = new Random();

    // field
    protected List<String> ids;

    /**
     * Initializes <code>GroupGenerator</code> object; converts ids into a List of Strings.
     *
     * @param ids   list of student IDs, as a String.
     * @see com.falcontechnologies.ChartGenerator
     * @see com.falcontechnologies.GroupGenerator
     */
    public GroupGenerator(String[] ids) {
        this.ids = new ArrayList<>(Arrays.asList(ids));
    }

    /**
     * Reads data from a CSV file (comma-separated values).
     *
     * @param fileName  the file location.
     * @return          the contents of the file, separated into lines and values between commas.
     */
    public static String[][] readFromCSV(String fileName) {
        TextFile file = new TextFile(fileName);
        
        String[] lines = file.read().split("\n");
        String[][] list = new String[lines.length][];
        for (int i = 0; i < lines.length; i++) {
            list[i] = lines[i].split(",");
        }
        
        return list;
    }

    /**
     * Updates the <code>.txt</code> files in the <code>charts</code> and <code>teams</code> directories.
     * <p>
     * For each ID in each array of the input 2D array, it removes all other IDs in the 1D array
     * from the corresponding line in the <code>.txt</code> file.
     *
     * @param fileName      the file location.
     * @param ids           the list of IDs; used to obtain line numbers.
     * @param newGroups     contains IDs, grouped into groups.
     * @see #generateMap(String)
     */
    public void updateCSV(String fileName, List<String> ids, String[][] newGroups) {
        TextFile file = new TextFile(fileName);

        String[] lines = file.read().split("\n");

        String text = "";
        for (int i = 0; i < lines.length; i++) {
            String id = ids.get(i);
            for (String[] table : newGroups)
                if (indexOf(table, id) != -1)
                    for (String person : table)
                        lines[i] = lines[i].replace(person + ",", "").replace("," + person, "");
            text += lines[i].replace(",,", ",") + "\n";
        }

        file.write(text);
    }

    /**
     * Reads from a file and generates a <code>Map</code> of <code>Strings</code> to <code>List</code> of
     * <code>Strings</code>. The keys are ID numbers, and the Lists are composed of every other ID number.
     *
     * @param fileName      the file location.
     * @return              the generated Map.
     * @see #readFromCSV(String)
     * @see #updateCSV(String, List, String[][])
     */
    public Map<String, List<String>> generateMap(String fileName) {
        String[][] list = readFromCSV(fileName);
        
        Map<String, List<String>> map = new HashMap<>();
        for (int i = 0; i < ids.size(); i++)
            if (!ids.get(i).equals("Empty"))
                map.put(ids.get(i), Arrays.asList(list[i]));

        return map;
    }

    /**
     * Returns the intersection of two <code>Lists</code>.
     * @param list1     the first <code>List</code>.
     * @param list2     the second <code>List</code>.
     * @param <T>       the <code>Object</code> type of the <code>Lists</code>.
     * @return          the intersection of the two <code>Lists</code>.
     */
    public <T> List<T> intersection(List<T> list1, List<T> list2) {
        List<T> list = new ArrayList<>();
        
        for (T t : list1) {
            if (list2.contains(t)) {
                list.add(t);
            } else if (t != null && t.equals("Empty") && !list.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }

    /**
     * Selects a random element of a <code>List</code> of <code>Strings</code>.
     * <p>
     * If no elements are left, it returns a <code>String</code> message.
     *
     * @param array     the <code>List</code> of elements.
     * @return          a random element of the array. if the array is empty, "No elements left".
     */
    public String getRandom(List<String> array) {
        try {
            return array.get(R.nextInt(array.size()));
        } catch (IndexOutOfBoundsException ex) {
            return "No elements left";
        }
    }

    // obtains the index of a String in a String[] array
    private int indexOf(String[] array, String e) {
        for (int i = 0; i < array.length; i++)
            if (e.equals(array[i]))
                return i;

        return -1;
    }

    /**
     * Writes to a file such that, for each line (which an ID from ids is assigned to),
     * every other ID is written, separated by commas.
     *
     * @param fileName      the file location.
     * @see #readFromCSV(String)
     * @see #updateCSV(String, List, String[][])
     */
    public void resetFile(String fileName) {
        TextFile file = new TextFile(fileName);
        String text = "";

        for (int i = 0; i < ids.size(); i++) {
            for (int j = 0; j < ids.size(); j++)
                if (i != j)
                    text += ids.get(j) + ",";

            text = text.substring(0, text.length() - 1) + "\n";
        }

        file.write(text);
    }

}