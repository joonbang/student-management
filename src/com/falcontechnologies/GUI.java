package com.falcontechnologies;

import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Joon Bang
 */
public class GUI extends JFrame {
	
	private static final long serialVersionUID = 1L;
	/**
     * Creates new form GUI
     */
    public GUI() {
        generationOption = 0; // 0 for alphabetically, 1 for randomly
        projectName = "Indirect Measurement";
    	
    	initComponents();
    	
    	try {
    		String[] text = new TextFile("files/previous/file_name.txt").read().split("\n");
    		this.filePath = text[0];
    		this.fileName = text[1];
    		this.fileNameBox.setText(fileName);
    	} catch (ArrayIndexOutOfBoundsException e) {
    		
    	}
    	
    	chartGroup.add(chartOptionA);
        chartGroup.add(chartOptionB);
    	
    	chartOptionA.setSelected(true);
    }
    
    // initialize components
    @SuppressWarnings("unchecked")
    private void initComponents() {

        chartGroup = new ButtonGroup();
        colorPanel = new JPanel();
        tabs = new JTabbedPane();
        importPanel = new JPanel();
        jLabel1 = new JLabel();
        fileNameBox = new JTextField();
        selectButton = new JButton();
        importButton = new JButton();
        chartPanel = new JPanel();
        jLabel2 = new JLabel();
        chartOptionA = new JRadioButton();
        chartOptionB = new JRadioButton();
        chartGenerateButton = new JButton();
        teamPanel = new JPanel();
        jLabel3 = new JLabel();
        projectNameBox = new JComboBox<>();
        teamGenerateButton = new JButton();
        assignScoresButton = new JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Student Management");
        setResizable(false);

        colorPanel.setBackground(new java.awt.Color(110, 234, 89));

        tabs.setBackground(new java.awt.Color(110, 234, 89));

        importPanel.setBackground(new java.awt.Color(110, 234, 89));

        jLabel1.setText("File Name:");

        fileNameBox.setEnabled(false);
        fileNameBox.setMaximumSize(new java.awt.Dimension(6, 20));

        selectButton.setBackground(new java.awt.Color(110, 234, 89));
        selectButton.setText("Select File");
        selectButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectButtonActionPerformed(evt);
            }
        });

        importButton.setBackground(new java.awt.Color(110, 234, 89));
        importButton.setText("Read from File");
        importButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                importButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout importPanelLayout = new javax.swing.GroupLayout(importPanel);
        importPanel.setLayout(importPanelLayout);
        importPanelLayout.setHorizontalGroup(
            importPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(importPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(importPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(importPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fileNameBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(importPanelLayout.createSequentialGroup()
                        .addComponent(selectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(importButton)
                        .addGap(0, 135, Short.MAX_VALUE)))
                .addContainerGap())
        );
        importPanelLayout.setVerticalGroup(
            importPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(importPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(importPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(fileNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(importPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectButton)
                    .addComponent(importButton))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        tabs.addTab("Import from Excel", importPanel);

        chartPanel.setBackground(new java.awt.Color(110, 234, 89));

        jLabel2.setText("Generation Options");

        chartOptionA.setBackground(new java.awt.Color(110, 234, 89));
        chartOptionA.setText("Alphabetical");
        chartOptionA.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                generationOption = 0;
            }
        });

        chartOptionB.setBackground(new java.awt.Color(110, 234, 89));
        chartOptionB.setText("Random");
        chartOptionB.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
            	generationOption = 1;
            }
        });

        chartGenerateButton.setBackground(new java.awt.Color(110, 234, 89));
        chartGenerateButton.setText("Generate");
        chartGenerateButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                chartGenerateButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chartPanelLayout = new javax.swing.GroupLayout(chartPanel);
        chartPanel.setLayout(chartPanelLayout);
        chartPanelLayout.setHorizontalGroup(
            chartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chartPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(chartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(chartPanelLayout.createSequentialGroup()
                        .addComponent(chartOptionA)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chartOptionB))
                    .addComponent(jLabel2)
                    .addComponent(chartGenerateButton))
                .addContainerGap(195, Short.MAX_VALUE))
        );
        chartPanelLayout.setVerticalGroup(
            chartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chartPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chartOptionA)
                    .addComponent(chartOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chartGenerateButton)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        tabs.addTab("Seating Chart", chartPanel);

        teamPanel.setBackground(new java.awt.Color(110, 234, 89));

        jLabel3.setText("Project:");

        projectNameBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Indirect Measurement", "Target Practice", "Chem R&E" }));
        projectNameBox.setMaximumSize(new java.awt.Dimension(28, 20));
        projectNameBox.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                projectNameBoxActionPerformed(evt);
            }
        });

        teamGenerateButton.setBackground(new java.awt.Color(110, 234, 89));
        teamGenerateButton.setText("Generate");
        teamGenerateButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                teamGenerateButtonActionPerformed(evt);
            }
        });

        assignScoresButton.setBackground(new java.awt.Color(110, 234, 89));
        assignScoresButton.setText("Assign Scores");
        assignScoresButton.setEnabled(false);
        assignScoresButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignScoresButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout teamPanelLayout = new javax.swing.GroupLayout(teamPanel);
        teamPanel.setLayout(teamPanelLayout);
        teamPanelLayout.setHorizontalGroup(
            teamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(teamPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(teamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(teamPanelLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(projectNameBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(teamPanelLayout.createSequentialGroup()
                        .addComponent(teamGenerateButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(assignScoresButton)
                        .addGap(0, 153, Short.MAX_VALUE)))
                .addContainerGap())
        );
        teamPanelLayout.setVerticalGroup(
            teamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(teamPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(teamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(projectNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(teamPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(teamGenerateButton)
                    .addComponent(assignScoresButton))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        tabs.addTab("Project Teams", teamPanel);

        javax.swing.GroupLayout colorPanelLayout = new javax.swing.GroupLayout(colorPanel);
        colorPanel.setLayout(colorPanelLayout);
        colorPanelLayout.setHorizontalGroup(
            colorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, colorPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(tabs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        colorPanelLayout.setVerticalGroup(
            colorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, colorPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(tabs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(colorPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(colorPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        
        pack();
        
        this.setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Enables/disables the assign scores button, and creates the teamGenerator object.
    private void projectNameBoxActionPerformed(ActionEvent evt) {
        this.projectName = (String)this.projectNameBox.getSelectedItem();

        try {
            int x, y;

            switch (projectName) {
                case "Indirect Measurement":
                    x = (int)Math.ceil(ids.length / 2.0);
                    y = 2;
                    assignScoresButton.setEnabled(false);
                    break;
                case "Target Practice":
                    x = (int)Math.ceil(ids.length / 4.0);
                    y = 4;
                    assignScoresButton.setEnabled(false);
                    break;
                default:
                    x = (int)Math.ceil(ids.length / 3.0);
                    y = 3;
                    assignScoresButton.setEnabled(true);
                    break;
            }
            teamGenerator = new TeamGenerator(generateFileName("teams/", ".txt"), ids, x, y);
        } catch (NullPointerException e) {
            projectNameBox.setSelectedItem("Indirect Measurement");
            JOptionPane.showMessageDialog(this, "Error: no file imported.");
        }
    }

    // Generates an alphabetical/random seating chart, based on generationOption, and displays it using a
    // ChartOutput object.
    private void chartGenerateButtonActionPerformed(ActionEvent evt) {
        if (ids == null) {
            JOptionPane.showMessageDialog(this, "Error: no file imported.");
            return;
        }

        String[][] chart;

        try {
            chart = chartGenerator.generateAlphabeticalChart();

            if (generationOption == 1)
                chart = chartGenerator.generateRandomChart();
        } catch (StackOverflowError e) {
            JOptionPane.showMessageDialog(this, "Error: seating chart "
                    + "generation failed;\nno more possible combinations of "
                    + "students.\nPlease try again.");
            reset(0);
            return;
        }

        ChartOutput out = new ChartOutput(chart, idsToName);
        out.setVisible(true);
        makeScreenshot(out, generateFileName("outputs/charts/", "chart.png"));
    }
    
    // Imports and reads the Excel file. Initializes fields relating to current file and generation.
    // If no file was selected using the select button, the Import button is by default a "Use Previous File" button.
    // In its default state, the button will attempt to import from the last file selected/imported.
    private void importButtonActionPerformed(ActionEvent evt) {
        String msg;

        // create some useful TextFiles
        TextFile previousFileName = new TextFile("files/previous/file_name.txt");
        TextFile previousFiles = new TextFile("files/previous/files.txt");
        
        if (this.fileName == null || this.fileName.equals("")) {
        	JOptionPane.showMessageDialog(this, "Error: no file was selected.");
        	return;
        }
        
        // dialog prompt for sheet name
        String sheetName = JOptionPane.showInputDialog(this, "Please enter"
                + " the sheet name: ");
        
        
        // error handling
        if (sheetName == null || sheetName.equals("")) {
            JOptionPane.showMessageDialog(this, "Error: no sheet name provided.");
            return;
        }
        try {
            // set some fields
            msg = "File successfully imported.";
            this.sheet = sheetName;

            // import from Excel
            ExcelReader reader = new ExcelReader(this.filePath);
            String text = reader.read(sheet);

            // create new files for new sheet
            new TextFile(generateFileName("student_data/", ".csv")).write(text);

            // set information
            String[] lines = text.split("\n");

            this.names = new String[lines.length];
            this.ids = new String[lines.length];

            for (int i = 0; i < lines.length; i++) {
                String[] pieces = lines[i].split(";");
                this.names[i] = pieces[0];
                this.ids[i] = pieces[1];
            }

            // create ids to name HashMap
            this.idsToName = new HashMap<>();

            for (int i = 0; i < names.length; i++)
                idsToName.put(ids[i], names[i]);

            idsToName.put("Empty", "");

            // create generators
            this.chartGenerator = new ChartGenerator(generateFileName("charts/", ".txt"), ids, 6, 5);
            this.teamGenerator = new TeamGenerator(generateFileName("teams/", ".txt"), ids, 15, 2);

            // update files
            previousFileName.write(this.filePath + "\n" + this.fileName);

            // Quits out of import if file has already been read, as there is
            // no need to reset / update the below files.
            if (previousFiles.read().contains(this.filePath + "_" + sheetName)) {
                JOptionPane.showMessageDialog(this, msg);
                return;
            }

            previousFiles.write(previousFiles.read() + this.filePath + "_" + this.sheet);

            // reset all files
            for (int o = 0; o < 3; o++)
                reset(o);
        } catch (IOException e) { // either sheet name doesn't exist, or previous file is not in same location
            if (e.getMessage().contains("Sheet DNE"))
                msg = "Error: cannot find sheet with given sheet name.";
            else
                msg = "Error: cannot find file. It may have been moved or deleted.";
        }

        JOptionPane.showMessageDialog(this, msg);
    }

    // Allows user to select the file to import, using a JFileChooser object.
    private void selectButtonActionPerformed(ActionEvent evt) {
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showOpenDialog(this);

        String msg;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            String path = file.getAbsolutePath();

            if (!path.endsWith(".xls")) {
                msg = "Error: file was not of type \".xls\".";
            } else {
                msg = "File successfully selected.";
                this.filePath = path;
                this.fileName = file.getName();
                this.fileNameBox.setText(fileName);
            }
        } else {
            msg = "No file was selected.";
        }

        JOptionPane.showMessageDialog(this, msg);
    }

    // Generates teams for projects, based on the selected project name, and displays it using a
    // TeamOutput object.
    private void teamGenerateButtonActionPerformed(ActionEvent evt) {
        if (ids == null) {
            JOptionPane.showMessageDialog(this, "Error: no file imported.");
            return;
        }

        projectNameBoxActionPerformed(evt);

        String[][] teams;
        try {
            switch (projectName) {
                case "Indirect Measurement":
                    teams = teamGenerator.generateRandomTeams();
                    break;
                case "Target Practice":
                    teams = teamGenerator.generateRandomTeams();
                    break;
                default:
                    try {
                    	int attempts = 0;
                    	while (true) { // keep trying to make teams until it is unfeasible
	                    	try {
		                        teamGenerator.assignStudentGroups(groups);
		                        teams = teamGenerator.generateTeamsBySkill();
		                        break;
	                    	} catch (StackOverflowError e) {
	                    		if (attempts > 1000000) {
	                    			throw new StackOverflowError();
	                    		} else {
	                    			attempts++;
	                    		}
	                    	}
                    	}
                    } catch (NullPointerException e) {
                        JOptionPane.showMessageDialog(this,
                                "Error: scores were not assigned.");
                        return;
                    }
            }
        } catch (StackOverflowError e) {
            JOptionPane.showMessageDialog(this, "Error: project teams "
                    + "generation failed;\nno more possible combinations of "
                    + "students.\nPlease try again.");
            reset(1);
            return;
        }
        
        // obtain project / team names
        String[] teamNames = new String[teams.length];

        if (projectName.equals("Chem R&E")) {
            teamNames = new TextFile("files/project_names.txt").read().split("\n");
        } else {
            for (int i = 0; i < teamNames.length; i++) {
                teamNames[i] = "Team #"+(i+1);
            }
        }
        
        // generate .txt output
        String text = "";
        text += projectName + " Teams\n";
        for (int i = 0; i < teams.length; i++) {
        	text += " * " + teamNames[i] + "\n";
        	for (int j = 0; j < teams[i].length; j++) {
        		text += "    * " + idsToName.get(teams[i][j]) + "\n";
        	}
        	text += "\n";
        }
        
        new TextFile(generateFileName("outputs/teams/", "_" + projectName + ".txt")).write(text);
        new TeamOutput(teams, projectName, idsToName).setVisible(true);
    }

    // Creates a ScoreGUI object, which allows the user to assign scores to the students.
    @SuppressWarnings("unchecked")
    private void assignScoresButtonActionPerformed(ActionEvent evt) {
        if (ids == null) {
            JOptionPane.showMessageDialog(this, "Error: no spreadsheet"
                    + " has been imported.");
            return;
        }

        new ScoreGUI(names, generateFileName("scores/", ".csv")).setVisible(true);

        String[][] scoresFromFile = GroupGenerator.readFromCSV(
                generateFileName("scores/", ".csv"));
        int[][] scores = new int[scoresFromFile.length][3];

        for (int i = 0; i < scoresFromFile.length; i++)
            for (int j = 0; j < scoresFromFile[i].length; j++)
                scores[i][j] = Integer.parseInt(scoresFromFile[i][j]);

        this.groups = new ArrayList[3];

        for (int i = 0; i < 3; i++)
            groups[i] = new ArrayList<>();

        for (int i = 0; i < scores.length; i++) {
            int max = Math.max(Math.max(scores[i][0], scores[i][1]),
                    scores[i][2]);

            for (int j = 0; j < 3; j++) {
                if (scores[i][j] == max) {
                    groups[j].add(ids[i]);
                    break;
                }
            }
        }
    }

    // Helper Methods
    /**
     * Resets files storing information about generation / scores.
     *
     * @param option    Which file to reset.
     *                  0 = seating chart files,
     *                  1 = project team files,
     *                  2 = student score files.
     * @see com.falcontechnologies.GroupGenerator
     */
    protected void reset(int option) {
        GroupGenerator generator = new GroupGenerator(ids);

        switch (option) {
            case 0:
                generator.resetFile(generateFileName("charts/", ".txt"));
                break;
            case 1:
                generator.resetFile(generateFileName("teams/", ".txt"));
                break;
            default:
                String text = "";
                for (int i = 0; i < ids.length; i++)
                    text += "2,2,2\n";
                new TextFile(generateFileName("scores/", ".csv")).write(text);
                break;
        }
    }

    /**
     * Creates a file name, based on the selected Excel file and spreadsheet.
     * Used for saving files.
     *
     * @param directory directory for the generated file name.
     * @param extension extension of the generated file name.
     * @return          the generated file name.
     * @see #importButtonActionPerformed(ActionEvent)
     * @see #reset(int)
     */
    protected String generateFileName(String directory, String extension) {
        return "files/" + directory + this.fileName + "_" + this.sheet + extension;
    }

    /**
     * Creates a screenshot of the JFrame object argFrame. 
     *
     * @param argFrame 		JFrame you want to make screenshot of.
     * @param outputFile 	location where you want to save the screenshot to.
     */
    protected static final void makeScreenshot(JFrame argFrame, String outputFile) {
        Rectangle rec = argFrame.getBounds();
        BufferedImage bufferedImage = new BufferedImage(rec.width, rec.height,
                BufferedImage.TYPE_INT_ARGB);
        argFrame.paint(bufferedImage.getGraphics());

        try {
            // Create the screenshot file.
            File output = new File(outputFile);
            // Use the ImageIO API to write the bufferedImage to the file
            ImageIO.write(bufferedImage, "png", output);
        } catch (IOException e) {
        	
        } // catch
    } // makeScreenshot method
    
    // Main
    /**
     * Main method. Creates the GUI.
     * @param args the command line arguments
     * @see #GUI()
     */
    public static void main(String args[]) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | 
        		UnsupportedLookAndFeelException ex) {
            Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        EventQueue.invokeLater(new Runnable() {
            @Override
			public void run() {
                new GUI().setVisible(true);
            }
        });
    }

    // Fields
    // information about the current file
    private String filePath;
    private String fileName;
    private String sheet;
    private String[] ids;
    private String[] names;

    // related to generation
    private ChartGenerator chartGenerator;
    private TeamGenerator teamGenerator;
    private int generationOption;
    private String projectName;

    // gui elements
    private JButton assignScoresButton;
    private JButton chartGenerateButton;
    private ButtonGroup chartGroup;
    private JRadioButton chartOptionA;
    private JRadioButton chartOptionB;
    private JPanel chartPanel;
    private JPanel colorPanel;
    private JTextField fileNameBox;
    private JButton importButton;
    private JPanel importPanel;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JComboBox<String> projectNameBox;
    private JButton selectButton;
    private JTabbedPane tabs;
    private JButton teamGenerateButton;
    private JPanel teamPanel;

    // other
    private Map<String, String> idsToName;
    private List<String>[] groups;
}