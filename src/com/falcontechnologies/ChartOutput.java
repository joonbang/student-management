package com.falcontechnologies;

import java.util.Map;
import javax.swing.*;

/**
 *
 * @author Joon Bang
 */
public class ChartOutput extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	// fields
    final static int WIDTH = 1200;
    final static int HEIGHT = 720;
    
    private String[][] names;
    private String[][] fullNames;

    /**
     * Initializes the fields. Creates a <code>ChartPanel</code> object.
     * @param seatingChart      2D <code>String</code> array of student IDs, arranged into tables.
     * @param idsToName         <code>Map</code> of student IDs to their name; Last, First.
     */
    public ChartOutput(String[][] seatingChart, Map<String, String> idsToName) {
        this.names = new String[seatingChart.length][seatingChart[0].length];
        this.fullNames = new String[seatingChart.length][seatingChart[0].length]; 
        
        for (int i = 0; i < seatingChart.length; i++) {
            for (int j = 0; j < seatingChart[i].length; j++) {
                String[] temp = idsToName.get(seatingChart[i][j]).split(", ");
                
                if (temp.length == 1) { // is "Empty"
                    this.names[i][j] = temp[0];
                    this.fullNames[i][j] = temp[0];
                } else {
                    this.names[i][j] = temp[1];
                    this.fullNames[i][j] = temp[1] + " " + temp[0];
                }
            }
        }
        
        initialize(seatingChart, names, fullNames);
    }

    // creates the ChartPanel object, sets properties of the JFrame
    private void initialize(String[][] array, String[][] array2, String[][] array3) {
        ChartPanel panel = new ChartPanel(array, array2, array3);
        
        panel.putStudentNames();
        
        this.add(panel);
        this.setSize(WIDTH, HEIGHT);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
}

